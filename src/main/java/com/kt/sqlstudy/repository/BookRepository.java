package com.kt.sqlstudy.repository;

import com.kt.sqlstudy.entity.Book;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface BookRepository extends PagingAndSortingRepository<Book, Long> {
}